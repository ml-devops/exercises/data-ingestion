
import pandas as pd
import numpy as np
import timeit
import subprocess
import pickle
import os
import json

##################Load config.json and get environment variables
with open('config.json','r') as f:
    config = json.load(f) 

dataset_csv_path = os.path.join(config['output_folder_path']) 
test_data_path = os.path.join(config['test_data_path'])
trained_model_path = os.path.join(config['output_model_path'])
model_name = config['model_name']

##################Function to get model predictions


def preprocess_data(df):
    df = df.drop(['corporation', 'exited'], axis=1)
    return df


def model_predictions(test_data_paths):
    #read the deployed model and a test dataset, calculate predictions
    trained_model = pickle.load(open(trained_model_path + model_name, 'rb'))
    test_data = os.listdir(test_data_paths)
    for data in test_data:
        if data.endswith('.csv'):
            test_data_df = pd.read_csv(test_data_paths + data, index_col=[0])
            y_test = test_data_df.iloc[:, -1].values
            #print("the arg provided", test_data_paths)
            #print("the input df from ingested", test_data_df)
            test_data_df = preprocess_data(test_data_df)
            x_test = test_data_df.values
            y_pred = trained_model.predict(x_test)
            return [y_pred, y_test]

        else:
            print("Check dataset path was entered correctly")


def dataframe_summary():
    dataset = os.listdir(test_data_path)
    for data in dataset:
        if data.endswith('.csv'):
            test_data_df = pd.read_csv(test_data_path+data)
            test_data_df = preprocess_data(test_data_df)
            test_data_df = test_data_df.agg(['std', 'mean', 'median'])
            list_out = [value for value in test_data_df.values]
    return list_out


def execution_time():
    start_time = timeit.default_timer()
    os.system("python ingestion.py")
    t_1 = timeit.default_timer() - start_time

    start_time_2 = timeit.default_timer()
    os.system("python training.py")
    t_2 = timeit.default_timer() - start_time_2

    time = list([t_1, t_2])
    return time


def outdated_packages_list():
    installed = subprocess.check_output(['pip', 'list', '--outdated'])
    with open('outdated_packages.txt', 'wb') as file:
        file.write(installed)
    with open('outdated_packages.txt', 'r') as file:
        outdated_packages = file.read()
    return outdated_packages


if __name__ == '__main__':
    predictions = model_predictions(test_data_paths=None)
    print("the predictions: ", predictions)
    dataframe_summary(test_data_path)
    execution_time()
    outdated_packages_list()





    
