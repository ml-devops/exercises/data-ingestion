


from training import train_model
from scoring import score_model
from deployment import store_model_into_pickle
from ingestion import merge_multiple_dataframe
import reporting
import apicalls
import json
import os


##################Check and read new data
#first, read ingestedfiles.txt

with open('config.json', 'r') as f:
    config = json.load(f)

ingested_data = os.path.join(config['output_folder_path'])
latest_score_path = os.path.join(config["prod_deployment_path"])
model_name = config['model_name']

with open(ingested_data+"ingested_files.json", 'r') as file:
    data_json = json.load(file)


def new_data():
    source_data = os.path.join(config["input_folder_path"])
    source_data = os.listdir(source_data)
    print(source_data)
    source_data = [data for data in source_data if data.endswith('.csv')]
    print("cleaned sd", source_data)
    for data in source_data:
        if data != data_json['file_name']:
            merge_multiple_dataframe()
            return True

        else:
            print("Didn't find new data")
            return False


def check_model_drift():
    with open(latest_score_path + "latest_score.txt", 'r') as file:
        current_score = file.read()

    new_score, new_f1 = score_model(test_data_path=ingested_data)

    print("current_score:", current_score, "new f1:", new_f1)

    if (float(new_f1)-float(current_score)) > 0.1:
        train_model()
        score_model(test_data_path=ingested_data)
        store_model_into_pickle(model_name=model_name)

    else:
        return


#second, determine whether the source data folder has files that aren't listed in ingestedfiles.txt



##################Deciding whether to proceed, part 1
#if you found new data, you should proceed. otherwise, do end the process here


##################Checking for model drift
#check whether the score from the deployed model is different from the score from the model that uses the newest ingested data
# this Tomorrow


##################Deciding whether to proceed, part 2
#if you found model drift, you should proceed. otherwise, do end the process here



##################Re-deployment
#if you found evidence for model drift, re-run the deployment.py script

##################Diagnostics and reporting
#run diagnostics.py and reporting.py for the re-deployed model

def main():
    if new_data():
        check_model_drift()
        reporting.score_model()
        apicalls.call_apis()




if __name__ == '__main__':
    main()





