import pandas as pd
import numpy as np
import os
import json
from datetime import datetime



with open('config.json','r') as f:
    config = json.load(f) 

input_folder_path = config['input_folder_path']
output_folder_path = config['output_folder_path']


def merge_multiple_dataframe():
    #check for datasets, compile them together, and write to an output file
    final_df = pd.DataFrame(columns=["corporation", "lastmonth_activity", "lastyear_activity", "number_of_employees","exited"])
    files = os.listdir(os.getcwd()+"/"+input_folder_path)
    for file in files:
        if file.endswith('csv'):
            df_partial = pd.read_csv(os.getcwd()+"/"+input_folder_path+"/"+file)
            final_df = final_df.append(df_partial)
        result = final_df.drop_duplicates()
        file_name = "final_data.csv"
        result.to_csv(output_folder_path+"/"+file_name)

    return file_name, len(final_df.index)


def records(file_name, dataset_len):
    date_object= datetime.now()
    time_now = str(date_object.year) + '/' + str(date_object.month) + '/' + str(date_object.day)
    all_records = {"output_folder_path": output_folder_path,
                   "file_name": file_name,
                   "data_set_length": dataset_len,
                   "Timestamp": time_now}

    records_json = json.dumps(all_records, indent=4)
    with open(output_folder_path+"/"+"ingested_files.json", 'w') as out_file:
        out_file.write(records_json)


if __name__ == '__main__':
    file_name, len_dataset = merge_multiple_dataframe()
    records(file_name=file_name, dataset_len=len_dataset)
