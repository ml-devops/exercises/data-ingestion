from flask import Flask, session, jsonify, request
import pandas as pd
import numpy as np
import pickle
import os
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
import json

###################Load config.json and get path variables
with open('config.json', 'r') as f:
    config = json.load(f)

dataset_csv_path = os.path.join(config['output_folder_path'])
model_path = os.path.join(config['output_model_path'])

with open(dataset_csv_path + "/" + "ingested_files.json", 'r') as out:
    ingested_data = json.load(out)

training_data = dataset_csv_path + "/" + ingested_data['file_name']

assert training_data is not None


def train_model():
    data_df = pd.read_csv(training_data)
    data_df = data_df.drop(['Unnamed: 0', 'corporation'], axis=1)
    for col in data_df.columns:
        print(col)
    print(data_df.head(2))
    # use this logistic regression for training
    logit = LogisticRegression(C=1.0, class_weight=None, dual=False, fit_intercept=True,
                       intercept_scaling=1, l1_ratio=None, max_iter=100,
                       multi_class='auto', n_jobs=None, penalty='l2',
                       random_state=0, solver='liblinear', tol=0.0001, verbose=0,
                       warm_start=False)

    x = data_df.loc[:, data_df.columns != 'exited'].values

    y = data_df.iloc[:, -1].values
    print("y: ", y.shape)
    # fit the logistic regression to your data
    model = logit.fit(x, y)

    # write the trained model to your workspace in a file called trainedmodel.pkl
    model_name = "trained_model.pkl"
    pickle.dump(model, open(model_path+model_name, 'wb'))

train_model()
