from flask import Flask, session, jsonify, request
import pandas as pd
import numpy as np
import pickle
from diagnostics import model_predictions, dataframe_summary, execution_time
from scoring import score_model
import json
import os



######################Set up variables for use in our script
app = Flask(__name__)
app.secret_key = '1652d576-484a-49fd-913a-6879acfa6ba4'

with open('config.json','r') as f:
    config = json.load(f) 

dataset_csv_path = os.path.join(config['output_folder_path'])


prediction_model = None


@app.route("/prediction", methods=['POST', 'OPTIONS'])
def predict():        
    #call the prediction function you created in Step 3
    dataset = request.args.get('dataset')
    y_pred = model_predictions(test_data_paths=dataset)
    print(y_pred)
    return jsonify({'predictions': [int(x) for x in y_pred[0]]})



@app.route("/scoring", methods=['GET','OPTIONS'])
def score():
    result, f1_score = score_model(dataset_csv_path)
    return jsonify({"f1_score": f1_score})



#######################Summary Statistics Endpoint
@app.route("/summarystats", methods=['GET','OPTIONS'])
def stats():
    statistics = str(dataframe_summary())
    return jsonify({"statistics": statistics})


# #######################Diagnostics Endpoint
@app.route("/diagnostics", methods=['GET','OPTIONS'])
def diagnostics():
    #check timing and percent NA values
    diagnostics_output = {"execution times": []}
    time = execution_time()
    diagnostics_output['execution times'] = time
    print(diagnostics_output)

    return jsonify(diagnostics_output)


if __name__ == "__main__":    
    app.run(host='0.0.0.0', port=8000, debug=True, threaded=True)
