from flask import Flask, session, jsonify, request
import pandas as pd
import numpy as np
import pickle
import os
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
import json



#################Load config.json and get path variables
with open('config.json','r') as f:
    config = json.load(f) 

dataset_csv_path = os.path.join(config['output_folder_path']) 
test_data_path = os.path.join(config['test_data_path'])
trained_model_path = os.path.join(config['output_model_path'])
model_name = config['model_name']


#################Function for model scoring
def score_model(test_data_path):
    #this function should take a trained model, load test data, and calculate an F1 score for the model relative to the test data
    #it should write the result to the latestscore.txt file

    trained_model = pickle.load(open(trained_model_path+model_name, 'rb'))
    test_data = os.listdir(test_data_path)
    for data in test_data:
        if data.endswith('.csv'):
            test_data_df = pd.read_csv(test_data_path+data, index_col=[0])
            #print("TEST DF:", "\n", test_data_df)
            test_data_df = test_data_df.drop(['corporation'], axis=1)
            #print(test_data_df.columns)
            #print("data to be used for scoring:      ", "\n",  test_data_df)

            x_test = test_data_df.loc[:, test_data_df.columns != 'exited'].values
            y_pred = trained_model.predict(x_test)

            y_test = test_data_df.iloc[:, -1].values
            result = trained_model.score(x_test, y_test)
            f1_score = metrics.f1_score(y_test, y_pred)

            print("result and f1", result, f1_score)
            with open("latest_score.txt", 'w') as out_file:
                out_file.write(str(f1_score))

            return result, f1_score


#score_model(test_data_path)