from flask import Flask, session, jsonify, request
import pandas as pd
import numpy as np
import pickle
import os
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
import shutil
import json



##################Load config.json and correct path variable
with open('config.json','r') as f:
    config = json.load(f) 

dataset_csv_path = os.path.join(config['output_folder_path']) 
prod_deployment_path = os.path.join(config['prod_deployment_path']) 
output_model_path = os.path.join(config['output_model_path'])
model_name = config['model_name']


####################function for deployment
def store_model_into_pickle(model_name):
    #copy the latest pickle file, the latestscore.txt value, and the ingestfiles.txt file into the deployment directory

    prod_artifacts = []
    model_src = output_model_path + model_name
    prod_artifacts.append(model_src)
    latest_score = os.getcwd() + "/" +"latest_score.txt"
    prod_artifacts.append(latest_score)
    records = os.listdir(dataset_csv_path)
    print("the records", records)
    for ingested_data in records:
        if ingested_data.endswith(".json"):
            prod_artifacts.append(str(dataset_csv_path+ingested_data))

    print("full list:", prod_artifacts)

    for artifact in prod_artifacts:
        print("the artifact:", artifact)
        shutil.copy(artifact, prod_deployment_path)


store_model_into_pickle(model_name=model_name)


        
        
        

