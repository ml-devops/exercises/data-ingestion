import requests
import os
import json

#Specify a URL that resolves to your workspace
URL = "http://127.0.0.1:8000/"

with open('config.json','r') as f:
    config = json.load(f)

test_data_path = os.path.join(config['test_data_path'])
trained_model_path = os.path.join(config['output_model_path'])
dataset_csv_path = os.path.join(config['output_folder_path'])


def call_apis():
    responses = {}
    responses['predictions'] = requests.post(f'{URL}prediction?dataset={dataset_csv_path}')
    responses['scoring'] = requests.get(f'{URL}scoring')
    responses['summarystats'] = requests.get(f'{URL}summarystats')
    responses['diagnostics'] = requests.get(f'{URL}diagnostics')

    with open(os.getcwd() + "/" + trained_model_path + "/api_returns2.txt", "w") as file:
         for resp in responses:
             file.write(json.dumps(responses[resp].json(), indent=4) + '\n')


#combine all API responses
    print("dictionary:", responses)




call_apis()
