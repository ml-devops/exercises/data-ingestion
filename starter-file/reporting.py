import pickle
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from diagnostics import model_predictions
import pandas as pd
import numpy as np
from sklearn import metrics
import matplotlib.pyplot as plt
import seaborn as sns
import json
import os



###############Load config.json and get path variables
with open('config.json','r') as f:
    config = json.load(f) 

dataset_csv_path = os.path.join(config['output_folder_path'])
test_data_path = os.path.join(config['test_data_path'])
trained_model_path = os.path.join(config['prod_deployment_path'])
model_name = config['model_name']




##############Function for reporting
def score_model():
    #calculate a confusion matrix using the test data and the deployed model
    #write the confusion matrix to the workspace
    #trained_model = pickle.load(open(trained_model_path + model_name, 'rb'))
    y_pred, y_true = model_predictions(dataset_csv_path)
    c_matrix = confusion_matrix(y_true, y_pred)
    fig = plt.figure()
    plt.matshow(c_matrix)
    plt.title('Confusion matrix -Dynamic  risk assessment')
    plt.colorbar()
    plt.ylabel('True Label')
    plt.xlabel('Predicted Label')
    plt.savefig(os.getcwd() + "/" + trained_model_path + "/confusion_matrix2.png")


if __name__ == '__main__':
    score_model()
