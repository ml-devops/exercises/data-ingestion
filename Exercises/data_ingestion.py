import os
import pandas as pd

directories = ['../data/']

print(os.getcwd())
print("folders in directory:", os.listdir(directories[0]))


def read_files(path):
    final_df = pd.DataFrame(columns=['peratio', 'price'])
    for folder in os.listdir(path):
        for file_name in os.listdir(path + folder):
            print(path + folder + "/" + file_name)

            current_df = pd.read_csv(path + folder + "/" + file_name)
            final_df = final_df.append(current_df).reset_index(drop=True)


    final_df.to_csv(path+"demo_csv.csv")


read_files(directories[0])
