# Dynamic Risk Assesment System 

The folder `starter-file`contains the completed scripts: 
- training.py
- scoring.py
- deployment.py
- ingestion.py
- diagnostics.py
- reporting.py
- app.py
- apicalls.py
- fullprocess.py
- finaldata.csv 
- ingestedfiles.txt 
- trainedmodel.pkl 
- latestcore.txt 
- confusionmatrix.png 
- apireturns.txt and apireturns2.txt 
- Crontab file

## Runing the project
 ``` cd starter-file```

 ``` python3 app.py & python3 fullprocess.py ```
